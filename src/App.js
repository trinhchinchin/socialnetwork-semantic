import React from 'react'
import './App.css'
import { 
  BrowserRouter as Router, 
  Switch, 
  Route, 
  Redirect 
} from 'react-router-dom'
import Login from './pages/Login'
import Signup from './pages/Signup'
import Index from './pages/Index'

import PrivateRoute from './pages/PrivateRoute'

class App extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path='/login' render={(props) => <Login {...props} />} />
          <Route exact path='/signup' render={(props) => <Signup {...props} />} />
          <PrivateRoute path='/' component={Index} />
          <Redirect to='/login' />
        </Switch>
      </Router>
    );
  }
}

export default App


