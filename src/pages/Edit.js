import React, { Component } from 'react'
import { Menu, Grid, Segment } from 'semantic-ui-react'
import { Route, Link, Switch } from 'react-router-dom'
import EditProfile from '../pages/EditProfile'
import EditPass from '../pages/EditPass'

export class Edit extends Component {
  state = { activeItem: 'edit profile' }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
    const { activeItem } = this.state

    return (
      <React.Fragment>

        <h5>Update information</h5>
        <Grid>
          <Grid.Column width={4}>
            <Menu pointing secondary vertical>
              <Menu.Item
                as={Link}
                to='/edit/profile'
                name='edit profile'
                active={activeItem === 'edit profile'}
                onClick={this.handleItemClick}
              />
              <Menu.Item
                as={Link}
                to='/edit/password'
                name='change password'
                active={activeItem === 'change password'}
                onClick={this.handleItemClick}
              />

            </Menu>
          </Grid.Column>

          <Grid.Column width={12}>
            <Segment>
              <Switch>
                <Route path='/edit/profile' component={EditProfile} />
                <Route path='/edit/password' component={EditPass} />
              </Switch>
            </Segment>

          </Grid.Column>
        </Grid>

      </React.Fragment>
    )
  }
}

export default Edit
