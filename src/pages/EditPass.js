import React, { Component } from 'react'
import { Form, Image, Button, Message, Grid } from 'semantic-ui-react'
import apiCaller from '../utils/apiCaller'

export class EditPass extends Component {
  state = {
    loading: true,
    avatar: '',
    name: '',
    oldpass: '',
    newpass: '',
    confirmnew: '',
    colormess: '',
    mess: ''
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  componentDidMount() {
    var userId = localStorage.getItem('userId')
    apiCaller(
      'POST',
      `api/user/${userId}`,
      null,
      null
    ).then(res => {
      // console.log(res)
      this.setState({
        avatar: res.data.data.avatar,
        name: res.data.data.name,
        loading: false
      })
    })
  }

  onSubmit = (e) => {
    e.preventDefault()
    // console.log(this.state)
    var userId = localStorage.getItem('userId')
    const { oldpass, newpass, confirmnew } = this.state

    if (confirmnew !== newpass) {
      this.setState({
        colormess: 'red',
        mess: 'Comfirm newpassword do not match.'
      })
      return
    }

    apiCaller(
      'POST',
      `api/user/password?id=${userId}&oldpass=${oldpass}&newpass=${newpass}`,
      null,
      null
    ).then(res => {
      // console.log(res)
      if (res.data.isSuccess === true) {
        this.setState({
          oldpass: '',
          newpass: '',
          confirmnew: '',
          colormess: 'green',
          mess: 'Successful.',
        })
      } else {
        this.setState({
          colormess: 'red',
          mess: res.data.message
        })
      }
    })
  }

  render() {
    const { loading, avatar, name, oldpass, newpass, confirmnew, colormess, mess } = this.state
    return (
      <React.Fragment>
        <Form onSubmit={this.onSubmit} loading={loading}>

          <Grid columns={2}>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column width='3' textAlign='right'>
                <Image src={avatar} avatar />
              </Grid.Column>
              <Grid.Column width='6'>
                <span><h5>{name}</h5></span>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column width='3' textAlign='right'>
                <label>Old Password</label>
              </Grid.Column>
              <Grid.Column width='6'>
                <Form.Input
                  name='oldpass'
                  onChange={this.onChange}
                  value={oldpass}
                  type='password' />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column width='3' textAlign='right'>
                <label>New Password</label>
              </Grid.Column>
              <Grid.Column width='6'>
                <Form.Input
                  name='newpass'
                  onChange={this.onChange}
                  value={newpass}
                  type='password' />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column width='3' textAlign='right'>
                <label>Comfirm New Password</label>
              </Grid.Column>
              <Grid.Column width='6'>
                <Form.Input
                  name='confirmnew'
                  onChange={this.onChange}
                  value={confirmnew}
                  type='password' />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column width='3' textAlign='right'>
              </Grid.Column>
              <Grid.Column width='6'>
                <Button
                  basic
                  content='Change Password' />
              </Grid.Column>
            </Grid.Row>
            {mess && (
              <Grid.Row verticalAlign='middle'>
              <Grid.Column width='3' textAlign='right'>
                </Grid.Column>
                <Grid.Column width='6'>
                  <Message color={colormess}>{mess}</Message>
                </Grid.Column>
              </Grid.Row>
            )}
          </Grid>


        </Form>
      </React.Fragment>
    )
  }
}

export default EditPass
