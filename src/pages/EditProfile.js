import React, { Component } from 'react'
import { 
  Form, 
  Image, 
  Label, 
  Button, 
  Message, 
  Grid 
} from 'semantic-ui-react'
import apiCaller from '../utils/apiCaller'

export class EditProfile extends Component {
  state = {
    loading: true,
    file: '',
    avatar: '',
    fname: '',
    uname: '',
    website: '',
    bio: '',
    email: '',
    phone: '',
    gender: false,
    colormess: '',
    mess: ''
  }

  onChangeFile = (e) => {
    var userId = localStorage.getItem('userId')
    this.setState({
      file: e.target.files[0]
    })
    if (e.target.files.length > 0) {
      const fd = new FormData()
      // Can not send if name files is not correct
      fd.append('files', e.target.files[0])
      fd.append('userId', userId)
      apiCaller(
        'POST',
        'api/user/avatar',
        fd,
        {
          'Content-type': 'multipart/form-data'
        }
      )
        .then(res => {
          // console.log(res)
          if (res.data.isSuccess === true) {
            this.setState({
              avatar: res.data.data.urlList[0],
              colormess: 'green',
              mess: 'Upload Avatar Successful.'
            })
          } else {
            this.setState({
              colormess: 'red',
              mess: res.data.message
            })
          }
        })
    }
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  onChangeGender = (e, { value }) => {
    this.setState({ gender: value })
  }

  componentDidMount() {
    var userId = localStorage.getItem('userId')
    apiCaller(
      'POST',
      `api/user/${userId}`,
      null,
      null
    ).then(res => {
      // console.log(res)
      this.setState({
        avatar: res.data.data.avatar,
        fname: res.data.data.name,
        uname: res.data.data.username,
        website: res.data.data.website,
        bio: res.data.data.bio,
        email: res.data.data.email,
        phone: res.data.data.phone,
        gender: res.data.data.gender,
        loading: false
      })
    })
  }

  onSubmit = (e) => {
    e.preventDefault()
    var userId = localStorage.getItem('userId')
    const { fname, website, bio, phone, gender } = this.state
    apiCaller(
      'POST',
      'api/user/update',
      {
        "id": userId,
        "name": fname,
        "website": website,
        "bio": bio,
        "phone": phone,
        "gender": gender
      },
      null
    ).then(res => {
      // console.log(res)
      if (res.data.isSuccess === true) {
        this.setState({
          fname: res.data.data.name,
          website: res.data.data.website,
          bio: res.data.data.bio,
          phone: res.data.data.phone,
          gender: res.data.data.gender,
          colormess: 'green',
          mess: 'Successful.'
        })
      } else {
        this.setState({
          colormess: 'red',
          mess: res.data.message
        })
      }
    })
  }
  render() {
    const { loading, avatar, fname, uname, website, bio, email, phone, gender, colormess, mess } = this.state
    const options = [
      { text: 'Female', value: 0 },
      { text: 'Male', value: 1 },
    ]
    return (
      <React.Fragment>
        <Form onSubmit={this.onSubmit} loading={loading}>
          <Grid columns={2}>

            <Grid.Row verticalAlign='middle'>
              <Grid.Column width='3' textAlign='right'>
                <Image src={avatar} avatar />
                <span><b>{uname}</b></span>
              </Grid.Column>
              <Grid.Column width='6'>
                <Label
                  as="label"
                  basic
                  htmlFor="upload"
                >
                  <Button
                    icon="upload"
                    label={{
                      basic: true,
                      content: 'Select file(s)'
                    }}
                    labelPosition="right"
                    disabled
                  />
                  <input
                    hidden
                    id="upload"
                    multiple
                    type="file"
                    onChange={this.onChangeFile}
                    accept="image/jpg, image/jpeg, image/png"
                  />
                </Label>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column width='3' textAlign='right'>
                <label>Full name</label>
              </Grid.Column>
              <Grid.Column width='6'>
                <Form.Input
                  name='fname'
                  onChange={this.onChange}
                  value={fname}
                  type='text' />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column width='3' textAlign='right'>
                <label>Username</label>
              </Grid.Column>
              <Grid.Column width='6'>
                <Form.Input
                  name='uname'
                  onChange={this.onChange}
                  value={uname}
                  readOnly
                  type='text' />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column width='3' textAlign='right'>
                <label>Website</label>
              </Grid.Column>
              <Grid.Column width='6'>
                <Form.Input
                  name='website'
                  onChange={this.onChange}
                  value={website}
                  type='text' />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column width='3' textAlign='right'>
                <label>Bio</label>
              </Grid.Column>
              <Grid.Column width='6'>
                <Form.TextArea
                  name='bio'
                  onChange={this.onChange}
                  value={bio}
                  type='text' />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column width='3' textAlign='right'>
                <label>Email</label>
              </Grid.Column>
              <Grid.Column width='6'>
                <Form.Input
                  name='email'
                  onChange={this.onChange}
                  value={email}
                  readOnly
                  type='text' />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column width='3' textAlign='right'>
                <label>Phone Number</label>
              </Grid.Column>
              <Grid.Column width='6'>
                <Form.Input
                  name='phone'
                  onChange={this.onChange}
                  value={phone}
                  type='text' />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column width='3' textAlign='right'>
                <label>Gender</label>
              </Grid.Column>
              <Grid.Column width='6'>
                <Form.Select
                  name='gender'
                  onChange={this.onChangeGender}
                  value={gender}
                  options={options} />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row verticalAlign='middle'>
              <Grid.Column width='3' textAlign='right'>
              </Grid.Column>
              <Grid.Column width='6'>
                <Button basic content='Submit' />
              </Grid.Column>
            </Grid.Row>
            {mess && (
              <Grid.Row verticalAlign='middle'>
                <Grid.Column width='3' textAlign='right'></Grid.Column>
                <Grid.Column width='6'>

                  <Message color={colormess}>{mess}</Message>

                </Grid.Column>
              </Grid.Row>
            )}
          </Grid>
        </Form>
      </React.Fragment>
    )
  }
}

export default EditProfile
