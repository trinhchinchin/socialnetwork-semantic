import React from 'react'
import {
  Dimmer,
  Loader,
  Grid,
  Image
} from 'semantic-ui-react'
import apiCaller from '../utils/apiCaller'
import { Link } from 'react-router-dom'

export class Explore extends React.Component {
  state = {
    loading: true,
    posts: '',
  }

  componentDidMount() {
    apiCaller(
      'POST',
      'api/post/explore',
      null,
      null
    ).then(res => {
      // console.log(res)
      this.setState({
        posts: res.data.data,
        loading: false
      })
    })
  }

  render() {
    const { loading, posts } = this.state
    // console.log(this.state)
    if (loading) {
      return (
        <Dimmer active inverted>
          <Loader size='large'>Loading</Loader>
        </Dimmer>
      )
    }

    return (
      <React.Fragment>

        <h5>Explore</h5>
        <Grid columns={3}>
          {posts && posts.map((post) => (
            <Grid.Column key={post.id}>
              <Link to={`/timeline/${post.userId}`}>
                <Image src={post.photos[0]} size='large'
                  style={{
                    height: '350px',
                    width: '100%',
                    objectFit: 'cover',
                    objectPosition: 'center'
                  }} />
              </Link>
            </Grid.Column>
          ))}
        </Grid>
      </React.Fragment>
    )
  }
}

export default Explore
