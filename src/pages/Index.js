import React from 'react'
import Navbar from '../pages/Navbar'
import { Route, Switch, Redirect } from 'react-router-dom'
import { Container } from 'semantic-ui-react'

import Newsfeed from './Newsfeed'
import Explore from './Explore'
import Timeline from './Timeline'
import Edit from './Edit'
import Messages from './Messages'

export class Index extends React.Component {


  render() {
    return (
      <React.Fragment>

        <Navbar />

        <div style={{ paddingTop: '80px' }}>
          <Container>

            {/* Switch */}
            <Switch>
              <Route exact path='/' render={(props) => <Newsfeed {...props} />} />
              <Route path='/explore' render={(props) => <Explore {...props} />} />
              <Route path='/timeline/:slug' render={(props) => <Timeline {...props} />} />
              <Route path='/edit' render={(props) => <Edit {...props} />} />
              <Route path='/messages/:slug' render={(props) => <Messages {...props} />} />

              <Redirect to='/' />
            </Switch>
          </Container>
        </div>

      </React.Fragment>
    )
  }
}

export default Index
