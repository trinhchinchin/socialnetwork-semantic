import React, { Component } from 'react'
import Auth from '../auth/Authenticate'
import {
  Form,
  Button,
  Icon,
  Dimmer,
  Loader,
  Grid,
  Segment,
  Header,
  Message,
  Image
} from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import apiCaller from '../utils/apiCaller'
import jwtDecode from 'jwt-decode'


export class Login extends Component {
  state = {
    loading: true,
    email: '',
    password: '',
    error: ''
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  componentWillMount() {
    var token = localStorage.getItem('token')
    if (token) {
      this.props.history.push('/')
    }
  }

  componentDidMount() {
    const { loading } = this.state
    if (loading) {
      setTimeout(() => this.setState({ loading: false }), 500)
    }
  }

  onSubmit = (e) => {
    e.preventDefault()
    const { email, password } = this.state

    apiCaller(
      'POST',
      'api/user/login',
      {
        "email": email,
        "password": password
      },
      null
    ).then(res => {
      // console.log(res)
      if (res.data.isSuccess === true) {
        localStorage.setItem('token', res.data.data.value)
        var userId = jwtDecode(res.data.data.value).nameid
        Auth.authenticate(() => {
          localStorage.setItem('userId', userId)
          this.props.history.push('/')
        })
      } else {
        this.setState({ error: res.data.message })
      }
    })
  }

  render() {
    const { loading, email, password, error } = this.state
    if (loading) {
      return (
        <Dimmer active inverted>
          <Loader size='large'>Loading</Loader>
        </Dimmer>
      )
    }
    return (
      <React.Fragment>
        <div style={{ paddingTop: '50px' }}>

          <Grid textAlign='center' style={{ height: '100%' }} verticalAlign='middle'>
            <Grid.Column style={{ maxWidth: 450 }}>
              <Form onSubmit={this.onSubmit} size='large'>

                <Segment padded='very'>
                  <Header as='h2' textAlign='center'>
                    <Image src='/logo/logo.png' size='big'/>
                  </Header>
                  <Form.Input
                    name='email'
                    onChange={this.onChange}
                    value={email}
                    fluid icon='at'
                    iconPosition='left'
                    placeholder='E-mail address'
                  />
                  <Form.Input
                    name='password'
                    onChange={this.onChange}
                    value={password}
                    fluid
                    icon='lock'
                    iconPosition='left'
                    placeholder='Password'
                    type='password'
                  />
                  <Button
                    basic
                    fluid
                    size='large'
                    animated='fade'>
                    <Button.Content visible>Login</Button.Content>
                    <Button.Content hidden>
                      <Icon name='sign-in' />
                    </Button.Content>
                  </Button>

                  {error && (
                    <Message color='red'>{error}</Message>
                  )}

                </Segment>
              </Form>

              <Segment>
                Don't have an account? <NavLink to='/signup'>Sign up</NavLink>
              </Segment>
            </Grid.Column>
          </Grid>

        </div>
      </React.Fragment>
    )
  }
}

export default Login
