import React from 'react'
import ReactDOM from 'react-dom'
import {
  Grid,
  Message
} from 'semantic-ui-react'

export class MessageItem extends React.Component {
  componentDidUpdate() {
    const node = ReactDOM.findDOMNode(this)
    node.scrollTop = node.scrollHeight
  }

  render() {
    const { histories, userId } = this.props
    return (
      <div style={{ top: 0, height: '300px', overflowY: 'scroll', overflowX: 'hidden' }}>
        {histories && histories.map((history) => {
          switch (history.senderId) {
            case parseInt(userId):
              return <Grid key={history.id}>
                <Grid.Row>
                  <Grid.Column width='10' floated='right' textAlign='right'>
                    <Message content={history.content} style={{ borderRadius: '20px' }} />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            default:
              return <Grid key={history.id}>
                <Grid.Row>
                  <Grid.Column width='10' textAlign='left'>
                    <Message content={history.content} style={{ borderRadius: '20px', backgroundColor: 'white' }} />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
          }
        })}
      </div>
    )
  }
}

export default MessageItem
