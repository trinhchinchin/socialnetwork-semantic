import React from 'react'
import {
  Grid,
  Segment,
  Button,
  Form,
  Icon,
  Image,
  Dimmer,
  Loader
} from 'semantic-ui-react'
import apiCaller from '../utils/apiCaller'
import MessageItem from './MessageItem'
import { Link } from 'react-router-dom'

export class Messages extends React.Component {
  state = {
    loading: true,
    user: '',
    histories: '',
    content: ''
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  onSend = () => {
    var slug = this.props.match.params.slug
    var userId = localStorage.getItem('userId')
    const { content } = this.state
    apiCaller(
      'POST',
      'api/message/chat',
      {
        "content": content,
        "senderId": userId,
        "receiverId": slug
      },
      null
    ).then(res => {
      // console.log(res)
      var copyHistories = [...this.state.histories]
      if (res.data.isSuccess === true) {
        this.setState({
          histories: [...copyHistories, res.data.data],
          content: ''
        })
      } else {
        console.log(res.data.message)
      }
    })
  }

  componentDidMount() {
    var slug = this.props.match.params.slug
    // Detail
    apiCaller(
      'POST',
      `api/user/${slug}`,
      null,
      null
    ).then(res => {
      // console.log(res)
      if (res.data.isSuccess === true) {
        this.setState({
          user: res.data.data,
          loading: false
        })
      } else {
        console.log(res.data.message)
      }
    })

    this.timer = this.lauchMessages();
  }

  lauchMessages = () => {
    var slug = this.props.match.params.slug
    var userId = localStorage.getItem('userId')
    this.timer = setInterval(() => (
      apiCaller(
        'POST',
        'api/message/history',
        {
          "senderId": userId,
          "receiverId": slug
        },
        null
      ).then(res => {
        // console.log(res)
        if (res.data.isSuccess === true) {
          this.setState({
            histories: res.data.data
          })
        } else {
          console.log(res.data.message)
        }
      })
    ), 200);
  }

  render() {
    const { loading, user, histories, content } = this.state
    var userId = localStorage.getItem('userId')
    // console.log(userId)
    // console.log(this.state)

    if (loading) {
      return (
        <Dimmer active inverted>
          <Loader size='large'>Loading</Loader>
        </Dimmer>
      )
    }
    return (
      <React.Fragment>
        <div style={{ paddingTop: '50px' }}>
          <Grid textAlign='center'>
            <Grid.Row>
              <Grid.Column width={12}>
                <Form>
                  <Segment.Group>
                    <Segment textAlign='center'>
                      <Image src={user.avatar} avatar />
                      {user && <Link to={`/timeline/${user.id}`} style={{ color: 'black' }}><b>{user.name}</b></Link>}
                    </Segment>
                    <Segment>

                      <MessageItem histories={histories} userId={userId} />

                    </Segment>
                    <Segment>
                      <Grid>
                        <Grid.Row>
                          <Grid.Column width='14'>
                            <Form.Input
                              name='content'
                              onChange={this.onChange}
                              value={content}
                              placeholder='Type a message...'
                            />
                          </Grid.Column>
                          <Grid.Column width='1'>
                            <Button
                              basic
                              size='large'
                              animated='fade'
                              onClick={this.onSend}
                            >
                              <Button.Content visible>Send</Button.Content>
                              <Button.Content hidden>
                                <Icon name='send' />
                              </Button.Content>
                            </Button>
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                    </Segment>
                  </Segment.Group>
                </Form>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </React.Fragment>
    )
  }
}

export default Messages
