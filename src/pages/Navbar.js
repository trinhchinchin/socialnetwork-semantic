import React from 'react'
import { Link } from 'react-router-dom'
import {
  Menu,
  Search,
  Container,
  Image,
  Grid,
  Dropdown,
  Comment,
  Message,
} from 'semantic-ui-react'
import apiCaller from '../utils/apiCaller'

export class Navbar extends React.Component {
  state = {
    isLoading: false,
    value: '',
    results: '',
    options: '',
    notifications: '',
  }

  componentWillMount() {
    this.resetComponent()
  }

  resetComponent = () => this.setState({ isLoading: false, results: [], value: '' })

  handleResultSelect = (e, { result }) => this.setState({ value: '' })

  handleSearchChange = (e, { value }) => {
    this.setState({ isLoading: true, value })
    var pattern = new RegExp("^[a-zA-Z0-9]+$");
    if (!pattern.test(value)) {
      this.setState({
        isLoading: false,
        results: []
      })
    } else {
      apiCaller(
        'GET',
        'api/user/name',
        null,
        null
      ).then(res => {
        // console.log(res.data)
        this.setState({
          options: res.data
        })
        setTimeout(() => {
          if (this.state.value.length < 1) return this.resetComponent()

          const re = new RegExp(value, 'i');

          const results = res.data.filter(u => re.test(u.title)).slice(0, 5).map(result => ({ ...result, description: result.id + '' }));
          // console.log(results)
          this.setState({
            isLoading: false,
            results: results,
          });
        }, 500)
      })
    }
  }

  resultRenderer({ id, title, image, description }) {
    return (
      <Link id={id} key={id} to={`/timeline/${description}`} style={{ color: 'black' }}>
        <Grid columns={2}>
          <Grid.Column width='4'>
            <Image src={image} size='tiny' avatar />
          </Grid.Column>
          <Grid.Column width='6'>
            <b>{title}</b>
          </Grid.Column>
        </Grid>
      </Link>
    )
  }

  componentDidMount() {
    var userId = localStorage.getItem('userId')
    apiCaller(
      'POST',
      `api/notification/${userId}`,
      null,
      null
    ).then(res => {
      // console.log(res)
      if (res.data.isSuccess === true) {
        this.setState({
          notifications: res.data.data
        })
      } else {
        console.log(res.data.message)
      }
    })
  }

  render() {
    const { isLoading, value, results, notifications } = this.state
    var userId = localStorage.getItem('userId')

    // console.log(username)
    return (
      <React.Fragment ref={this.handleContextRef}>

        <Menu fixed='top' borderless>
          <Container>
            <Menu.Item
              as={Link}
              to='/'>
              <Image src='/logo/logo.png' size='mini' />
            </Menu.Item>
            <Menu.Item
              as={Link}
              to='/'
              style={{
                fontFamily: 'Dancing Script,cursive',
                fontWeight: 'normal',
                fontSize: 'large',
                color: 'black',
              }}>Chnirtgram
              </Menu.Item>
            <Menu.Item>
              <Search
                loading={isLoading}
                onResultSelect={this.handleResultSelect}
                onSearchChange={this.handleSearchChange}
                resultRenderer={this.resultRenderer}
                results={results}
                value={value}
                placeholder={"Search"}

              />
            </Menu.Item>

            {/* <div style={{ height: '400px', overflowY: 'scroll'}}></div> */}
            <Menu.Menu position='right'>
              <Menu.Item
                as={Link}
                to='/explore'
                icon={{
                  name: 'compass outline',
                  size: 'large'
                }}
              />
              <Dropdown
                item
                floating
                direction='left'
                icon={{
                  name: 'heart outline',
                  size: 'large'
                }}
                style={{
                  zIndex: 1
                }}
              >
                <Dropdown.Menu>
                  <Dropdown.Header>Notifications</Dropdown.Header>
                  <Dropdown.Divider />

                  {notifications === ''
                    ? <Dropdown.Item>
                      <Message
                        warning
                        size='large'
                        header={`You don't have any notifications!`}
                        content='Comeback later...'
                      />
                    </Dropdown.Item>
                    : <div style={{ height: '220px', overflowY: 'scroll' }}>
                      <Comment.Group>
                        {notifications && notifications.map((notification, i) =>
                          <Dropdown.Item key={i} style={{ width: '270' }}>
                            <Comment>
                              <Comment.Avatar src={notification.avatar} />
                              <Comment.Content>
                                <Comment.Author
                                  as={Link}
                                  to={`/timeline/${notification.userId}`}
                                  style={{ color: 'black' }}>
                                  {notification.name}
                                </Comment.Author>
                                <Comment.Metadata>
                                  <div>{notification.isCreated}</div>
                                </Comment.Metadata>
                                <Comment.Text>{notification.content.slice(0, 27)}...</Comment.Text>
                              </Comment.Content>
                            </Comment>
                          </Dropdown.Item>
                        )}
                      </Comment.Group>
                    </div>
                  }

                </Dropdown.Menu>
              </Dropdown>
              <Menu.Item
                as={Link}
                to={`/timeline/${userId}`}
                icon={{
                  name: 'user outline',
                  size: 'large'
                }} />
            </Menu.Menu>
          </Container>
        </Menu>

      </React.Fragment>
    )
  }
}

export default Navbar
