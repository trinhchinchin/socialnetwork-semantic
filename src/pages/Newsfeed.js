import React from 'react'
import {
  Segment,
  Image,
  Icon,
  Grid,
  Sticky,
  Header,
  Dimmer,
  Loader,
  Form,
  Button,
  Message,
  Tab,
  Label,
} from 'semantic-ui-react'
import apiCaller from '../utils/apiCaller'
import { Picker } from 'emoji-mart'
import { Link } from 'react-router-dom'
import InfiniteScroll from 'react-infinite-scroller'

export class Newsfeed extends React.Component {
  state = {
    loading: true,
    // Upload
    files: '',
    urlList: '',
    idList: [],
    content: '',
    activeIndex: 0,
    disablePost: true,

    //Posts
    visible: 2,

    // Info
    user: '',

    //Suggestions
    suggestions: '',

    //Your List
    yourList: '',
    //Post
    posts: '',

    //Mess
    colormess: '',
    mess: '',
  }

  handleContextRef = contextRef => this.setState({ contextRef })

  onChangeFile = (e) => {
    var userId = localStorage.getItem('userId')
    this.setState({
      files: e.target.files
    })
    if (e.target.files.length > 0) {
      const fd = new FormData()
      // Can not send if name files is not correct
      Array.from(e.target.files).forEach(file => fd.append('files', file))
      fd.append('userId', userId)
      apiCaller(
        'POST',
        'api/upload',
        fd,
        {
          'Content-type': 'multipart/form-data'
        }
      )
        .then(res => {
          // console.log(res)
          if (res.data.isSuccess === true) {
            this.setState({
              files: '',
              urlList: res.data.data.urlList,
              idList: res.data.data.idList,
              colormess: 'green',
              mess: 'Upload Complete.',
              disablePost: false
            })
          } else {
            this.setState({
              colormess: 'red',
              mess: res.data.message
            })
          }
        })
    }
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  addEmoji = (e) => {
    this.setState({ content: this.state.content + e.native })
  }

  handleTabChange = (e, { activeIndex }) => this.setState({ activeIndex })

  onReset = () => {
    this.setState({
      content: '',
      urlList: [],
      disablePost: true,
      activeIndex: 0,
      colormess: '',
      mess: ''
    })
  }

  onSubmit = (e) => {
    e.preventDefault()
    var userId = localStorage.getItem('userId')
    const { content, idList, posts } = this.state

    apiCaller(
      'POST',
      'api/post/create',
      {
        "content": content,
        "userId": userId,
        "photoIds": idList
      },
      null
    ).then(res => {
      // console.log(res.data.data)
      if (res.data.isSuccess === true) {
        this.setState({
          posts: [res.data.data, ...posts],
          content: '',
          urlList: [],
          disablePost: true,
          activeIndex: 0,
          colormess: '',
          mess: ''
        })
      } else {
        this.setState({
          colormess: 'red',
          mess: res.data.message
        })
      }
    })
  }

  onLike = (postId) => {
    // console.log(postId)
    var userId = localStorage.getItem('userId')
    apiCaller(
      'POST',
      `api/post/like?postId=${postId}&viewerId=${userId}`,
      null,
      null
    ).then(res => {
      // console.log(res)
      const copyPosts = [...this.state.posts]
      if (res.data.isSuccess === true) {
        copyPosts.find(cp => cp.id === postId).isLike = 1
        copyPosts.find(cp => cp.id === postId).likeCount += 1
      } else {
        copyPosts.find(cp => cp.id === postId).isLike = 0
        copyPosts.find(cp => cp.id === postId).likeCount -= 1
      }
      this.setState({
        posts: copyPosts
      })
    })
  }

  onSave = (postId) => {
    console.log(postId)
    var userId = localStorage.getItem('userId')
    apiCaller(
      'POST',
      `api/post/save?postId=${postId}&viewerId=${userId}`,
      null,
      null
    ).then(res => {
      // console.log(res)
      const copyPosts = [...this.state.posts]
      if (res.data.isSuccess === true) {
        copyPosts.find(cp => cp.id === postId).isSave = 1
      } else {
        copyPosts.find(cp => cp.id === postId).isSave = 0
      }
      this.setState({
        posts: copyPosts
      })
    })
  }

  loadMore = (postId) => {
    // console.log(postId)
    const copyPosts = [...this.state.posts]
    copyPosts.find(cp => cp.id === postId).visible += 10
    this.setState({
      posts: copyPosts
    })
  }

  loadFunc = () => {
    const { visible, posts } = this.state
    if (visible < posts.length) {
      this.setState({ visible: this.state.visible + 1 })
    }
  }

  onKeyPress = (e, postId) => {
    if (e.key === 'Enter') {
      var comment = this.refs[postId].value
      var userId = localStorage.getItem('userId')

      if (comment.length !== 0) {
        console.log(postId + userId + comment)

        apiCaller(
          'POST',
          'api/post/comment',
          {
            "content": comment,
            "postId": postId,
            "userId": userId
          },
          null
        ).then(res => {
          console.log(res)
          const copyPosts = [...this.state.posts]
          if (res.data.isSuccess === true) {
            var copyComments = copyPosts.find(cp => cp.id === postId).comments
            copyPosts.find(cp => cp.id === postId).commentValue = ''
            copyPosts.find(cp => cp.id === postId).comments = [...copyComments, res.data.data]
            copyPosts.find(cp => cp.id === postId).commentCount += 1

            this.setState({
              posts: copyPosts,
              comment: ''
            })

            this.refs[postId].value = ''
          } else {
            console.log(res.data.message)
          }
        })
      }

    }
  }

  componentDidMount() {
    var userId = localStorage.getItem('userId')

    // Info
    apiCaller(
      'POST',
      `api/user/${userId}`,
      null,
      null
    ).then(res => {
      // console.log(res)
      this.setState({
        user: res.data.data,
      })
    })

    // Suggestions
    apiCaller(
      'POST',
      `api/user/random?userId=${userId}`,
      null,
      null
    ).then(res => {
      // console.log(res)
      this.setState({
        suggestions: res.data.data,
      })
    })

    // Your List
    apiCaller(
      'POST',
      `api/user/followingrandom?userId=${userId}`,
      null,
      null
    ).then(res => {
      // console.log(res)
      this.setState({
        yourList: res.data.data,
      })
    })

    // Posts
    apiCaller(
      'GET',
      `api/post?viewerId=${userId}`,
      null,
      null
    ).then(res => {
      // console.log(res)
      this.setState({ posts: res.data })
      setTimeout(() => this.setState({ loading: false }), 500)
    })
  }

  render() {
    const { loading, contextRef, user, urlList, content, suggestions, yourList, posts, colormess, mess, activeIndex, disablePost, visible } = this.state
    // console.log(this.state)
    const panes = [
      {
        menuItem: { key: 'image', icon: 'image outline' },
        render: () =>
          <Tab.Pane>
            <Label
              as="label"
              basic
              htmlFor="upload"
            >
              <Button
                icon="upload"
                label={{
                  basic: true,
                  content: 'Select file(s)'
                }}
                labelPosition="right"
                disabled
              />
              <input
                hidden
                id="upload"
                // multiple
                type="file"
                onChange={this.onChangeFile}
                accept="image/jpg, image/jpeg, image/png" />
            </Label>
            {urlList.length > 0 &&
              <Segment>

                <Image.Group size='small'>
                  {/* <pre style={{ overflow: 'auto' }}> */}
                  {urlList.map((file, index) =>
                    <Image key={index} src={file} />
                  )}
                  {/* </pre> */}
                </Image.Group>

              </Segment>
            }
            {/* Message */}
            {mess && (
              <Message color={colormess}>{mess}</Message>
            )}
          </Tab.Pane>,
      },
      {
        menuItem: { key: 'emoji', icon: 'fort awesome alternate' },
        render: () =>
          <Tab.Pane>
            <Picker onSelect={this.addEmoji} />
          </Tab.Pane>,
      },
    ]

    const TabExampleCustomMenuItem = () => <Tab panes={panes} activeIndex={activeIndex} onTabChange={this.handleTabChange} />

    if (loading) {
      return (
        <Dimmer active inverted>
          <Loader size='large'>Loading</Loader>
        </Dimmer>
      )
    }
    return (
      <React.Fragment>

        <h5>Newsfeed</h5>
        <div ref={this.handleContextRef}>
          <Grid columns={2}>
            <Grid.Row stretched>
              <Grid.Column width='11' >
                {/*Add Post */}
                <Form onSubmit={this.onSubmit}>
                  <Segment.Group>
                    <Segment>
                      <Grid>
                        {/* TextArea Post */}
                        <Grid.Row>
                          <Grid.Column width='12'>
                            <Form.TextArea
                              rows={1}
                              name='content'
                              onChange={this.onChange}
                              value={content}
                              placeholder={`What's you mind, ${user.name}?`} />
                          </Grid.Column>
                          <Grid.Column width='4' textAlign='right'>
                            <Button
                              basic
                              size='large'
                              animated='fade'
                              color='grey'
                              disabled={disablePost}
                            >
                              <Button.Content visible>Publish</Button.Content>
                              <Button.Content hidden>
                                <Icon name='paper plane outline' />
                              </Button.Content>
                            </Button>
                            {!disablePost &&
                              <span onClick={this.onReset}>
                                <Icon link name='close' />
                              </span>
                            }

                          </Grid.Column>
                          {/* <Grid.Column width='1'>
                            <Icon link name='close' />
                          </Grid.Column> */}
                        </Grid.Row>

                        {/* Icon Upload, Emoji */}
                        <Grid.Row>
                          <Grid.Column>
                            <TabExampleCustomMenuItem />
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                    </Segment>
                  </Segment.Group>
                </Form>

                {/* Posts */}
                  <InfiniteScroll
                    pageStart={0}
                    loadMore={this.loadFunc}
                    hasMore={true}
                    loader={<Loader key={0} active inline='centered' />}
                  >

                    {posts && posts.slice(0, visible).map((post) =>
                      <Segment.Group key={post.id}>
                        <Segment>
                          <Image src={post.avatar} avatar
                            style={{
                              height: '30px',
                              width: '30px',
                              objectFit: 'cover',
                              objectPosition: 'center'
                            }}
                          />
                          <Link to={`/timeline/${post.userId}`} style={{ color: 'black' }}>
                            <b>{post.name}</b>
                          </Link>
                        </Segment>

                        <Image src={post.photos[0]} size='huge' />

                        <Segment>
                          <Grid>
                            <Grid.Row>
                              <Grid.Column width='8'>
                                <span onClick={() => this.onLike(post.id)}>
                                  <Icon link name={post.isLike ? 'heart' : 'heart outline'} size='large' />
                                </span>
                                <Icon link name='comment outline' size='large' />
                              </Grid.Column>

                              <Grid.Column width='8' textAlign='right'>
                                <span onClick={() => this.onSave(post.id)}>
                                  <Icon link name={post.isSave ? 'bookmark' : 'bookmark outline'} size='large' />
                                </span>
                              </Grid.Column>
                            </Grid.Row>
                          </Grid>
                          <b>
                            {post.likeCount}
                            {post.likeCount > 1
                              ? ' likes '
                              : ' like '
                            }
                            {post.commentCount}
                            {post.commentCount > 1
                              ? ' comments '
                              : ' comment '
                            }
                          </b><br />
                          <Grid.Row>
                            <Grid.Column>
                              <b>{post.username}</b> {post.content}
                            </Grid.Column>
                          </Grid.Row>

                          {post.visible < post.comments.length && (
                            <span style={{ color: 'grey' }} onClick={() => this.loadMore(post.id)}>
                              View all {post.commentCount} comments
                          </span>
                          )}
                          {post.comments.length > 0 && post.comments.slice(-post.visible).map((comment) =>
                            <div key={comment.id}><b>{comment.username}</b> {comment.content}</div>
                          )}
                          <span style={{ color: 'grey' }}>{post.isCreated}</span><br />
                        </Segment>
                        <Segment>
                          <Grid>
                            <Grid.Row verticalAlign='middle'>
                              <Grid.Column width='15'>
                                <div className="ui input" style={{ width: '100%' }}>
                                  <input
                                    type='text'
                                    ref={post.id}
                                    onKeyPress={e => this.onKeyPress(e, post.id)}
                                    placeholder='Add a comment...' />
                                </div>

                              </Grid.Column>
                              <Grid.Column width='1' floated='left'>
                                <Icon name='ellipsis horizontal' size='large' />
                              </Grid.Column>
                            </Grid.Row>
                          </Grid>
                        </Segment>
                      </Segment.Group>
                    )}

                  </InfiniteScroll>

              </Grid.Column>
              <Grid.Column width='5'>
                <Sticky
                  context={contextRef}
                  offset={110}
                  style={{
                    zIndex: 2
                  }}>
                  {/* Info */}
                  <Segment>
                    <Link to={`timeline/${user.id}`} style={{ color: 'black' }}>
                      <Image src={user.avatar} avatar
                        style={{
                          height: '30px',
                          width: '30px',
                          objectFit: 'cover',
                          objectPosition: 'center'
                        }} />
                      <b>{user.name}</b>
                    </Link>

                  </Segment>

                  {/* Suggestions */}
                  <Segment.Group>
                    <Segment>
                      <Header as='h5' disabled>Suggestions</Header>
                    </Segment>
                    {suggestions && suggestions.map((person, index) =>
                      <Segment key={index}>
                        <Link to={`/timeline/${person.id}`} style={{ color: 'black' }}>
                          <Image src={person.avatar} avatar
                            style={{
                              height: '30px',
                              width: '30px',
                              objectFit: 'cover',
                              objectPosition: 'center'
                            }} />
                          <b>{person.name}</b>
                        </Link>
                      </Segment>
                    )}
                  </Segment.Group>

                  {/* Your List */}
                  <Segment.Group>
                    <Segment>
                      <Header as='h5' disabled>Your List</Header>
                    </Segment>
                    {yourList && yourList.map((person, index) =>
                      <Segment key={index}>
                        <Link to={`/timeline/${person.id}`} style={{ color: 'black' }}>
                          <Image src={person.avatar} avatar
                            style={{
                              height: '30px',
                              width: '30px',
                              objectFit: 'cover',
                              objectPosition: 'center'
                            }} />
                          <b>{person.name}</b>
                        </Link>
                      </Segment>
                    )}
                  </Segment.Group>
                </Sticky>

              </Grid.Column>
            </Grid.Row>
          </Grid>

        </div>
      </React.Fragment>
    )
  }
}

export default Newsfeed
