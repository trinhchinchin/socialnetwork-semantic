import React, { Component } from 'react'
import {
  Form,
  Button,
  Icon,
  Message,
  Segment,
  Dimmer,
  Loader,
  Grid,
  Header
} from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'
import apiCaller from '../utils/apiCaller'

export default class Signup extends Component {
  state = {
    loading: true,
    email: '',
    fname: '',
    uname: '',
    pass: '',
    confirmpass: '',
    colormess: '',
    mess: ''
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  onSubmit = (e) => {
    e.preventDefault()
    const { email, fname, uname, pass, confirmpass } = this.state

    if (confirmpass !== pass) {
      this.setState({
        colormess: 'red',
        mess: 'Comfirm password do not match.'
      })
      return
    }

    apiCaller(
      'POST',
      'api/user/register',
      {
        "email": email,
        "name": fname,
        "username": uname,
        "password": pass
      },
      null
    ).then(res => {
      // console.log(res)
      if (res.data.isSuccess === true) {
        this.setState({
          email: '',
          fname: '',
          uname: '',
          pass: '',
          confirmpass: '',
          colormess: 'green',
          mess: 'Successful.',
        })
      } else {
        this.setState({
          colormess: 'red',
          mess: res.data.message
        })
      }
    })
  }

  componentWillMount() {
    var token = localStorage.getItem('token')
    if (token) {
      this.props.history.push('/index')
    }
  }

  componentDidMount() {
    const { loading } = this.state
    if (loading) {
      setTimeout(() => this.setState({ loading: false }), 500)
    }
  }

  render() {
    const { loading, email, fname, uname, pass, confirmpass, colormess, mess } = this.state
    if (loading) {
      return (
        <Dimmer active inverted>
          <Loader size='large'>Loading</Loader>
        </Dimmer>
      )
    }
    return (
      <React.Fragment>
        <div style={{ paddingTop: '50px' }}>

          <Grid textAlign='center' style={{ height: '100%' }} verticalAlign='middle'>
            <Grid.Row>
              <Grid.Column style={{ maxWidth: 450 }}>

                <Form onSubmit={this.onSubmit} size='large'>
                  <Segment padded='very'>
                    <Header as='h3' disabled>Sign up to see photos <br /> from your friends.</Header>

                    <Form.Input
                      name='email'
                      onChange={this.onChange}
                      value={email}
                      fluid icon='at'
                      iconPosition='left'
                      placeholder='E-mail address'
                    />
                    <Form.Input
                      name='fname'
                      onChange={this.onChange}
                      value={fname}
                      fluid icon='address card'
                      iconPosition='left'
                      placeholder='Full name'
                    />
                    <Form.Input
                      name='uname'
                      onChange={this.onChange}
                      value={uname}
                      fluid icon='user'
                      iconPosition='left'
                      placeholder='Username'
                    />
                    <Form.Input
                      name='pass'
                      onChange={this.onChange}
                      value={pass}
                      fluid
                      icon='lock'
                      iconPosition='left'
                      placeholder='Password'
                      type='password'
                    />
                    <Form.Input
                      name='confirmpass'
                      onChange={this.onChange}
                      value={confirmpass}
                      fluid
                      icon='lock'
                      iconPosition='left'
                      placeholder='Confirm-Password'
                      type='password'
                    />
                    <Button
                      basic
                      fluid
                      size='large'
                      animated='fade'>
                      <Button.Content visible>Register</Button.Content>
                      <Button.Content hidden>
                        <Icon name='signup' />
                      </Button.Content>
                    </Button>

                    {mess && (
                      <Message color={colormess}>{mess}</Message>
                    )}

                  </Segment>
                </Form>
                <Segment>
                  Have an account? <NavLink to='/login'>Login</NavLink>
                </Segment>
              </Grid.Column>
            </Grid.Row>

          </Grid>
        </div>
      </React.Fragment>
    )
  }
}
