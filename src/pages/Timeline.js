import React from 'react'
import Auth from '../auth/Authenticate'
import {
  Grid,
  Loader,
  Button,
  Icon,
  Image,
  Segment,
  Tab,
  Dimmer,
  Message,
} from 'semantic-ui-react'
import apiCaller from '../utils/apiCaller'

export class Timeline extends React.Component {
  state = {
    loading: true,
    slug: '',
    user: '',
    posts: '',
    savedposts: '',
  }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  componentWillUpdate(nextProps) {
    var userId = localStorage.getItem('userId')
    var nextSlug = nextProps.match.params.slug
    if (nextProps.match.params.slug !== this.props.match.params.slug) {

      // Info
      apiCaller(
        'POST',
        `api/user/detail?userId=${nextSlug}&viewerId=${userId}`,
        null,
        null
      ).then(res => {
        // console.log(res)
        this.setState({
          user: res.data.data
        })
      })

      // Posts by userId
      apiCaller(
        'POST',
        `api/post/${nextSlug}`,
        null,
        null
      ).then(res => {
        // console.log(res)
        if (res.data.isSuccess === true) {
          this.setState({ posts: res.data.data })
        } else {
          console.log(res.data.message)
        }
      })

      // Saved Posts by userId
      apiCaller(
        'POST',
        `api/post/savelist?viewerId=${nextSlug}`,
        null,
        null
      ).then(res => {
        // console.log(res)
        if (res.data.isSuccess === true) {
          this.setState({ savedposts: res.data.data })
        } else {
          console.log(res.data.message)
        }
      })
    }
  }

  componentDidMount() {
    var userId = localStorage.getItem('userId')
    var slug = this.props.match.params.slug
    const { loading } = this.state

    // Info
    apiCaller(
      'POST',
      `api/user/detail?userId=${slug}&viewerId=${userId}`,
      null,
      null
    ).then(res => {
      // console.log(res)
      if (res.data.isSuccess === true) {
        this.setState({
          user: res.data.data
        })
      } else {
        console.log(res.data.message)
        this.props.history.push(`/timeline/${userId}`)
      }
    })

    // Posts by userId
    apiCaller(
      'POST',
      `api/post/${slug}`,
      null,
      null
    ).then(res => {
      // console.log(res)
      if (res.data.isSuccess === true) {
        this.setState({ posts: res.data.data })
      } else {
        console.log(res.data.message)
      }
    })

    // Saved Posts by userId
    apiCaller(
      'POST',
      `api/post/savelist?viewerId=${slug}`,
      null,
      null
    ).then(res => {
      // console.log(res)
      if (res.data.isSuccess === true) {
        this.setState({ savedposts: res.data.data })
      } else {
        console.log(res.data.message)
      }
    })

    if (loading) {
      setTimeout(() => this.setState({ loading: false }), 1000)
    }
  }

  onLogout = () => {
    localStorage.clear()
    Auth.logout(() => {
      this.props.history.push('/login')
    })
  }

  goEditProfile = () => {
    this.props.history.push('/edit/profile')
  }

  onMessage = () => {
    var slug = this.props.match.params.slug
    this.props.history.push(`/messages/${slug}`)
  }

  onFollow = (userId) => {
    // console.log(userId)
    var viewerId = localStorage.getItem('userId')
    apiCaller(
      'POST',
      `api/user/follow?userId=${userId}&viewerId=${viewerId}`,
      null,
      null
    ).then(res => {
      // console.log(res)
      const copyUser = this.state.user
      if (res.data.isSuccess === true) {
        copyUser.isFollow = 1
        copyUser.follower += 1
      } else {
        copyUser.isFollow = 0
        copyUser.follower -= 1
      }
      this.setState({
        user: copyUser
      })
    })
  }

  render() {
    const { loading, user, posts, savedposts } = this.state
    var userId = localStorage.getItem('userId')
    var slug = this.props.match.params.slug
    // console.log(userId)
    const panes = [
      {
        menuItem: { key: 'posts', icon: 'grid layout', content: 'Posts' },
        render: () =>
          <Tab.Pane>
            {posts === ''
              ? <Grid>
                <Grid.Column>
                  <Message
                    warning
                    size='big'
                    icon='warning'
                    header={`You don't have any posts!`}
                    content='Post something for your friends...'
                  />
                </Grid.Column>
              </Grid>
              : <Grid columns={3}>
                {posts && posts.map((post) => (
                  <Grid.Column key={post.id}>
                    <Image src={post.photos[0]} size='large'
                      style={{
                        height: '350px',
                        width: '100%',
                        objectFit: 'cover',
                        objectPosition: 'center'
                      }} />
                  </Grid.Column>
                ))}
              </Grid>
            }
          </Tab.Pane>
      },
      {
        menuItem: { key: 'saved', icon: 'bookmark outline', content: 'Saved' },
        render: () =>
          <Tab.Pane>
            {savedposts === ''
              ? <Grid>
                <Grid.Column>
                  <Message
                    warning
                    textAlign='center'
                    size='big'
                    icon='warning'
                    header={`You don't have any saved posts!`}
                    content={`Let's save anything you interested in...`}
                  />
                </Grid.Column>
              </Grid>
              : <Grid columns={3}>
                {savedposts && savedposts.map((savedpost) => (
                  <Grid.Column key={savedpost.id}>
                    <Image src={savedpost.photos[0]} size='large'
                      style={{
                        height: '350px',
                        width: '100%',
                        objectFit: 'cover',
                        objectPosition: 'center'
                      }} />
                  </Grid.Column>
                ))}
              </Grid>
            }
          </Tab.Pane>
      },
    ]

    const TabExampleCustomMenuItem = () => <Tab panes={panes} />

    if (loading) {
      return (
        <Dimmer active inverted>
          <Loader size='large'>Loading</Loader>
        </Dimmer>
      )
    }

    return (
      <React.Fragment>
        {/* Logout */}
        {slug === userId &&
          <Button
            onClick={this.onLogout}
            basic
            size='large'
            animated='fade'>
            <Button.Content visible>Logout</Button.Content>
            <Button.Content hidden>
              <Icon name='sign-out' />
            </Button.Content>
          </Button>
        }

        <h5>Timeline</h5>
        {/* Information */}
        <Grid columns={2}>
          <Grid.Row>
            {/* Avatar */}
            <Grid.Column width='4' verticalAlign='middle'>
              <Image src={user.avatar} circular centered
                style={{
                  height: '200px',
                  width: '200px',
                  objectFit: 'cover',
                  objectPosition: 'center'
                }} />
            </Grid.Column>

            {/* Info */}
            <Grid.Column width='8'>
              <Segment>
                <Grid>
                  <Grid.Row columns={2} verticalAlign='middle'>
                    <Grid.Column width='6'>
                      <b>{user.name}</b>
                    </Grid.Column>

                    <Grid.Column width='10'>
                      {slug === userId
                        ? <Button basic icon='setting' content='Edit Profile' onClick={this.goEditProfile} />
                        : user.isFollow
                          ? <div>
                            <Button basic icon='rss' content='Following' onClick={() => this.onFollow(user.id)} />
                            <Button basic icon='mail' content='Message' onClick={() => this.onMessage(user.id)} />
                          </div>
                          : <div>
                            <Button basic icon='plus' content='Follow' onClick={() => this.onFollow(user.id)} />
                            <Button basic icon='mail' content='Message' onClick={() => this.onMessage(user.id)} />
                          </div>
                      }

                    </Grid.Column>

                  </Grid.Row>

                  <Grid.Row columns={3}>
                    <Grid.Column width='5'>
                      {posts.length}
                      {posts.length > 1
                        ? ' posts '
                        : ' post '
                      }
                    </Grid.Column>
                    <Grid.Column width='5'>
                      {user.follower}
                      {user.follower > 1
                        ? ' followers'
                        : ' follower'}
                    </Grid.Column>
                    <Grid.Column width='6'>{user.following} following</Grid.Column>
                  </Grid.Row>
                  <Grid.Row>
                    <Grid.Column width='9'>
                      <h5>{user.username}</h5>
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row>
                    <Grid.Column width='9'>
                      <h5>{user.bio}</h5>
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row>
                    <Grid.Column width='9'>
                      <h5>{user.website}</h5>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </Segment>
            </Grid.Column>
          </Grid.Row>
        </Grid>

        <br />
        <TabExampleCustomMenuItem />

      </React.Fragment>
    )
  }
}

export default Timeline
